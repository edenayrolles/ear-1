package org.jacademie.firstwar.Servelet;


import org.jacademie.firstejb.EJB.RandomEJBStatelessSessionBean;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by edenayrolles on 20/11/2015.
 */
@WebServlet(name = "HelloServlet", urlPatterns="/")
public class HelloServlet extends HttpServlet {

    @EJB
    RandomEJBStatelessSessionBean randomEJBStatelessSessionBean;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher disp = this.getServletContext()
                .getRequestDispatcher("/index.jsp");
        request.setAttribute("randomNb", randomEJBStatelessSessionBean.random());

        disp.forward(request, response);
    }

}
