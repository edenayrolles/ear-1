package org.jacademie.firstejb.EJB;
import javax.ejb.Stateless;

/**
 * Created by edenayrolles on 20/11/2015.
 */
@Stateless(name = "RandomEJBStatelessSessionEJB")
public class RandomEJBStatelessSessionBean {
    public RandomEJBStatelessSessionBean() {
    }

    public double random() {
        return Math.random() * 100;
    }
}
